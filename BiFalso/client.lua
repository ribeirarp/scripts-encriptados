ESX                           = nil  
local PlayerData              = {}  
local complete                = true
local phoneModel              = "prop_amb_phone"
local primeiroNome            = ""
local segundoNome             = ""

Citizen.CreateThread(function()
    while ESX == nil do
      TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
      Citizen.Wait(0)
    end
    while ESX.GetPlayerData().job == nil do
      Citizen.Wait(10)
    end
    PlayerData = ESX.GetPlayerData()
end)
  
RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
    PlayerData = xPlayer    
end)

RegisterNetEvent('esx_bifalso:mudarNome')
AddEventHandler('esx_bifalso:mudarNome', function(xPlayer) 
    PlayerData = xPlayer   
    ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'number', {
        title = "Primeiro nome"
    }, function(data, menu)
        primeiroNome = data.value
        if primeiroNome ~= nil then
            menu.close()
            ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'number', {
                title = "Segundo nome"
            }, function(data2, menu2)
                segundoNome = data2.value
                if segundoNome ~= nil then
                    TriggerServerEvent('esx_bifalso:usarcartao', primeiroNome, segundoNome)               
                    menu2.close() 
                else
                    ESX.ShowNotification("Segundo nome ~r~inválido.~s~")
                end
                
                
            end, function(data2, menu2)
                menu2.close()
            end)
        else
            ESX.ShowNotification("Primeiro nome ~r~inválido.~s~")
        end
    end, function(data, menu)
        menu.close()
    end)
end)

Citizen.CreateThread(function()

    while true do
        Citizen.Wait(0)
        if complete ~= true then
            DisableAllControlActions(0)
        end
    end

end)


function LoadPropDict(model)
    while not HasModelLoaded(GetHashKey(model)) do
      RequestModel(GetHashKey(model))
      Wait(10)
    end
  end

RegisterNetEvent('esx_bifalso:success')
AddEventHandler('esx_bifalso:success', function(newNum)
    if Config.UseAnimation then
        -- PROGRESS START
        complete = false
        --TaskStartScenarioInPlace(, "WORLD_HUMAN_TOURIST_MOBILE", 0, false)
        prop1 = "prop_amb_phone"
        local Player = PlayerPedId()
        local x,y,z = table.unpack(GetEntityCoords(Player))
      
        if not HasModelLoaded(prop1) then
          LoadPropDict(prop1)
        end
      
        prop = CreateObject(GetHashKey(prop1), x, y, z+0.2,  true,  true, true)
        AttachEntityToEntity(prop, Player, GetPedBoneIndex(Player, 28422), 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, true, true, false, true, 1, true)
        SetModelAsNoLongerNeeded(prop1)


        local dict = "anim@cellphone@in_car@ds"

        RequestAnimDict(dict)
        while (not HasAnimDictLoaded(dict)) do Citizen.Wait(0) end
        
        TaskPlayAnim(GetPlayerPed(-1), dict, "cellphone_call_in", 2.0, 0.0, -1, 48, 2, 0, 0, 0 )
        exports['progressBars']:startUI(5000-500, "A ligar ao RicFazeres")
        Citizen.Wait(5000)
        exports['progressBars']:startUI(12000, "A explicar a situation ")
        Citizen.Wait(12000)
        complete = true
        DeleteEntity(prop)
        ClearPedTasksImmediately(GetPlayerPed(-1))
        ResetPedMovementClipset(PlayerPedId())  
        -- PROGRESS END        
        if complete then
            ESX.ShowNotification("O RicFazeres disse que hackeou o sistema e que tens um novo nome!")
        end   
    else
        TriggerServerEvent('esx_bifalso:mudarNome', newNum)   
        ESX.ShowNotification('Novo número de telemóvel ~g~' .. newNum)
        Citizen.Wait(3000)                             
        if Config.gcphoneEnabled then
            TriggerServerEvent('gcPhone:allUpdate')
        end  
    end
end)


