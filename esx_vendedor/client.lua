ESX = nil

local store = { 
    [_U('open_store')] = { title="Pharmacy", size= 50, colour=34, id=0, x = 17.21, y = 4343.23, z = 41.68}, 
} 

local NPC = {
   {seller = true, model = "s_m_y_blackops_01", x = 17.21, y = 4343.23, z = 40.67, h = 234.41},
}
  


Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()
	for k, v in pairs(store) do
		exports.ft_libs:AddArea("esx_vendedor:Blip" .. v.x, {
			marker = {
				type = 27,
				weight = 1,
				height = 1,
				red = 255,
				green = 0,
				blue = 0,
				showDistance = 2
			},
			trigger = {
				weight = 1,
				active = {
					callback = function()
						exports.ft_libs:HelpPromt("Pressiona ~INPUT_PICKUP~ para falar com o vendedor")
						if IsControlPressed(0, 38) then
							OpenPharmacyMenu()
						end
					end,
				},
			},
			locations = {
				{
					x = v.x,
					y = v.y,
					z = v.z,
				},
			},
		})
	end
end)

function OpenPharmacyMenu()
	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'pharmacy', {
		title    = 'Ze Ric',
		align = 'right',
		elements = {
			{label = 'Colete - 50.000€', value = 'kevlar'}
		}
	}, function(data, menu)
		TriggerServerEvent('esx_vendedorz:giveItem', data.current.value)
	end, function(data, menu)
		menu.close()
	end)
end

