local Keys = {
 ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57, 
 ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177, 
 ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
 ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
 ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
 ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70, 
 ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
 ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
 ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}
   
ESX = nil
local PlayerData              = {}

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
		PlayerData = ESX.GetPlayerData()
	end
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
    PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
end)

function hintToDisplay(text)
	SetTextComponentFormat("STRING")
	AddTextComponentString(text)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

  
local gym = {  
    {x = 870.58, y = -2312.57, z = 30.57}   
}




function OpenPawnMenu()
    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'pawn_menu',
        {
            title    = 'shhhhhh',
            elements = {
				--{label = 'Affär', value = 'shop'},
				{label = 'Comprar', value = 'sell'},
            }
        },
        function(data, menu)
            if data.current.value == 'shop' then
				OpenPawnShopMenu()
            elseif data.current.value == 'sell' then
				OpenSellMenu()
            end
        end,
        function(data, menu)
            menu.close()
        end
    )
end

function OpenSellMenu()
    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'pawn_sell_menu',
        {
            title    = 'Queres comprar alguma coisa?',
            elements = {
                {label = 'Molde da Pistola Vintage [ 30000€ ]'         ,value = 'molde_vintagepistol',      value2 = 30000},
                {label = 'Molde da Pistola SNS [ 25000€ ]'             ,value = 'molde_snspistol',          value2 = 25000},
                {label = 'Molde da Tec9 [ 40000€ ]'                    ,value = 'molde_machinepistol',      value2 = 40000},
                {label = 'Molde da Deagle [ 35000€ ]'                  ,value = 'molde_pistol50',           value2 = 35000},
                {label = 'Molde da Shotgun [ 70000€ ]'                 ,value = 'molde_sawnoffshotgun',     value2 = 70000},
                {label = 'Molde da shotgun de cano duplo [ 75000€ ]'   ,value = 'molde_dbshotgun',          value2 = 75000},
                {label = 'Molde da Gusenberg [ 125000€ ]'              ,value = 'molde_gusenberg',          value2 = 125000},
                {label = 'Molde da Mini AK [ 125000€ ]'                ,value = 'molde_compactrifle',       value2 = 125000},
                {label = 'Molde da Famas [ 127000€ ]'                  ,value = 'molde_assaultsmg',         value2 = 127000},
                {label = 'Molde da AK-47 [ 300000€ ]'                  ,value = 'molde_assaultrifle',       value2 = 300000},
                {label = 'Molde do Musket [ 350000€ ]'                 ,value = 'molde_musket',             value2 = 350000},
                
                --[[{label = 'Speaker (32kr)', value = 'speaker'},
                {label = 'Laptop (32kr)', value = 'laptop'},
                {label = 'Book (32kr)', value = 'book'},
                {label = 'Coupon (32kr)', value = 'coupon'},
                {label = 'Toothpaste (1)', value = 'toothpaste'},
                {label = 'Lottery Ticket (1000)', value = 'lotteryticket'},
                {label = 'Shirt(5000)', value = 'shirt'},
                {label = 'Pants(2000)', value = 'pants'},]]--
            }
        },
        function(data, menu)
            TriggerServerEvent('esx_ma:comprar', data.current.value, data.current.value2)
            
			--[[elseif data.current.value == 'speaker' then
				TriggerServerEvent('esx_moldearmas:sellspeaker')
            elseif data.current.value == 'laptop' then
				TriggerServerEvent('esx_moldearmas:selllaptop')
            elseif data.current.value == 'book' then
				TriggerServerEvent('esx_moldearmas:sellbook')
            elseif data.current.value == 'coupon' then
				TriggerServerEvent('esx_moldearmas:sellcoupon')
            elseif data.current.value == 'toothpaste' then
                TriggerServerEvent('esx_moldearmas:selltoothpaste')
            elseif data.current.value == 'lotteryticket' then
                TriggerServerEvent('esx_moldearmas:selllotteryticket')
            elseif data.current.value == 'shirt' then
                TriggerServerEvent('esx_moldearmas:sellshirt')
            elseif data.current.value == 'pants' then
                TriggerServerEvent('esx_moldearmas:sellpants')
            end]]--
        end,
        function(data, menu)
            menu.close()
        end
    )
end





RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()
    for k in pairs(gym) do
        exports.ft_libs:AddArea("gym_" .. gym[k].x , {
            marker = {
                type = 27,
                weight = 1,
                height = 1,
                red = 255,
                green = 255,
                blue = 255,
            },
            trigger = {
                weight = 1,
                active = {
                    callback = function()
                        exports.ft_libs:HelpPromt("Pressione ~INPUT_CONTEXT~ para abrir a ~b~loja~w~")
                        if IsControlJustPressed(0, Keys['E']) then
                            OpenPawnMenu()
                        end	
                    end,
                },
            },
            locations = {
                {
                    x = gym[k].x,
                    y = gym[k].y,
                    z = gym[k].z+0.1,
                }
            },
        })
    end
end)



