ESX = nil
PlayerData = nil
showHint = false

place = {
    {x = -559.40, y = 4192.56, z = 199.02},    
}

local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57, 
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177, 
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70, 
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}


Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end

	PlayerData = ESX.GetPlayerData()
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
    PlayerData = xPlayer
    for y in pairs(place) do
        for k in pairs(Config.allowedJobs) do
            if PlayerData.job.name == Config.allowedJobs[k] then
                exports.ft_libs:EnableArea("esx_blacmoney:blip_" .. y)
                break
            else
                exports.ft_libs:DisableArea("esx_blacmoney:blip_" .. y)
            end
        end
    end
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
    PlayerData.job = job
    for y in pairs(place) do
        for k in pairs(Config.allowedJobs) do
            if PlayerData.job.name == Config.allowedJobs[k] then
                exports.ft_libs:EnableArea("esx_blacmoney:blip_" .. y)
                break
            else
                exports.ft_libs:DisableArea("esx_blacmoney:blip_" .. y)
            end
        end
    end
end)


function hintToDisplay(text)
	SetTextComponentFormat("STRING")
	AddTextComponentString(text)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()
    for k in pairs(place) do
        exports.ft_libs:AddArea("esx_blacmoney:blip_".. k , {
            marker = {
                type = 27,
                weight = 1.0,
                height = 0.5,
                red = 255,
                green = 255,
                blue = 255,
                showDistance = 5,
                enable = false,
            },
            trigger = {
                weight = 1.0,
                exit = {
                    callback = function()
                        TriggerServerEvent('esx_blackmoney:leftMarker')
                    end,
        
                },
                
                active = {
                    callback = function()
                        hintToDisplay('Pressiona ~INPUT_CONTEXT~ para lavar ~r~dinheiro sujo')
                        if IsControlJustReleased(0, 38) and GetVehiclePedIsIn(GetPlayerPed(-1), false) == 0 then
                            TriggerServerEvent('esx_blackmoney:washMoney')
                        end
                    end,
                },
            },
            locations = {
                {
                    x = place[k].x, 
                    y = place[k].y,
                    z = place[k].z,
                },
            },
        })	
    end
end)

Citizen.CreateThread(function()
	while true do
		 Citizen.Wait(30000)
		 collectgarbage()
	end
end)