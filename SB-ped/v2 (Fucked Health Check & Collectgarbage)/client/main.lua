ESX = nil
Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent(Config.ESXTrigger, function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

RegisterNetEvent('sb-ped:mudarPed')
AddEventHandler('sb-ped:mudarPed', function(ped)
	playerHealth = GetEntityHealth(GetPlayerPed(-1))
	local hash = GetHashKey(ped)
	RequestModel(hash)
	while not HasModelLoaded(hash) do
		RequestModel(hash)
		Citizen.Wait(0)
	end
	SetPlayerModel(PlayerId(), hash)
	TriggerEvent('esx:restoreLoadout')
	SetEntityHealth(PlayerPedId(), playerHealth)
end)


RegisterNetEvent("sb-ped:reset")
AddEventHandler("sb-ped:reset", function()
	playerHealth = GetEntityHealth(GetPlayerPed(-1))
	TriggerEvent('skinchanger:loadDefaultModel', 0, function()
		ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin)
			TriggerEvent('skinchanger:loadSkin', skin)
			TriggerEvent('esx:restoreLoadout')
		end)
	end)
	SetEntityHealth(PlayerPedId(), playerHealth)
end)

Citizen.CreateThread(function()
	while true do
		 Citizen.Wait(30000)
		 collectgarbage()
	end
end)