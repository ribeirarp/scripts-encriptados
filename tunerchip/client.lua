-- Original Author: Elipse
-- Modified by: ModFreakz
-- For support, previews and showcases, head to https://discord.gg/ukgQa5K

local menu = false
ESX = nil
function print_table(node)
    local cache, stack, output = {},{},{}
    local depth = 1
    local output_str = "{\n"

    while true do
        local size = 0
        for k,v in pairs(node) do
            size = size + 1
        end

        local cur_index = 1
        for k,v in pairs(node) do
            if (cache[node] == nil) or (cur_index >= cache[node]) then

                if (string.find(output_str,"}",output_str:len())) then
                    output_str = output_str .. ",\n"
                elseif not (string.find(output_str,"\n",output_str:len())) then
                    output_str = output_str .. "\n"
                end

                -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
                table.insert(output,output_str)
                output_str = ""

                local key
                if (type(k) == "number" or type(k) == "boolean") then
                    key = "["..tostring(k).."]"
                else
                    key = "['"..tostring(k).."']"
                end

                if (type(v) == "number" or type(v) == "boolean") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = "..tostring(v)
                elseif (type(v) == "table") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = {\n"
                    table.insert(stack,node)
                    table.insert(stack,v)
                    cache[node] = cur_index+1
                    break
                else
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = '"..tostring(v).."'"
                end

                if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                else
                    output_str = output_str .. ","
                end
            else
                -- close the table
                if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                end
            end

            cur_index = cur_index + 1
        end

        if (size == 0) then
            output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
        end

        if (#stack > 0) then
            node = stack[#stack]
            stack[#stack] = nil
            depth = cache[node] == nil and depth + 1 or depth - 1
        else
            break
        end
    end

    -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
    table.insert(output,output_str)
    output_str = table.concat(output)

    print(output_str)
end


function getVehData(veh)
    if not DoesEntityExist(veh) then return nil end
    local lvehstats = {
        boost = GetVehicleHandlingFloat(veh, "CHandlingData", "fInitialDriveForce"),
        fuelmix = GetVehicleHandlingFloat(veh, "CHandlingData", "fDriveInertia"),
        braking = GetVehicleHandlingFloat(veh ,"CHandlingData", "fBrakeBiasFront"),
        drivetrain = GetVehicleHandlingFloat(veh, "CHandlingData", "fDriveBiasFront"),
        brakeforce = GetVehicleHandlingFloat(veh, "CHandlingData", "fBrakeForce")
    }
    return lvehstats
end

function setVehData(veh,data)
    if not DoesEntityExist(veh) or not data then return nil end
    SetVehicleEnginePowerMultiplier(veh, (data.gearchange + data.boost + data.fuelmix)*1.0)
    TriggerServerEvent('tuning:SetData',data,ESX.Game.GetVehicleProperties(veh))
end

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

function toggleMenu(b,send)
    menu = b
    SetNuiFocus(b,b)
    local vehData = getVehData(GetVehiclePedIsIn(GetPlayerPed(-1),false))
	local plate = GetVehicleNumberPlateText(GetVehiclePedIsIn(GetPlayerPed(-1)))
    
    ESX.TriggerServerCallback('tuning:getStats', function(veh)
		if (#veh.maximos <= 2) then
			TriggerServerEvent('tuning:SetDataMaximos',vehData,plate)
		else
			print("Ja tinha caralho")
		end
		

	end, plate)
	

	
	Citizen.Wait(3000)
	
	ESX.TriggerServerCallback('tuning:getStats', function(veh)
		if send then 
			SendNUIMessage(({type = "togglemenu", state = b, data = vehData, maximos = veh.maximos, tunerdata=veh.tunerdata})) 
		end
	end, plate)
	

	
end

RegisterNUICallback("togglemenu",function(data,cb)
    toggleMenu(data.state,false)
end)

RegisterNUICallback('getMaxs', function(data, cb)
    
	cb(itemCache[itemId])
end)


RegisterNUICallback("save",function(data,cb)
    local veh = GetVehiclePedIsIn(GetPlayerPed(-1),false)
    if not IsPedInAnyVehicle(GetPlayerPed(-1)) or GetPedInVehicleSeat(veh, -1)~=GetPlayerPed(-1) then return end
    setVehData(veh,data)
    lastVeh = veh
    lastStats = stats
end)




RegisterNetEvent("tuning:useLaptop")
AddEventHandler("tuning:useLaptop", function()
    if not menu then
        TriggerEvent('esx_inventoryhud:doClose')
		exports['progressBars']:startUI(6000, "A abrir computador")
        Citizen.Wait(3000)
        local ped = GetPlayerPed(-1)
        toggleMenu(true,true)
        while IsPedInAnyVehicle(ped, false) and GetPedInVehicleSeat(GetVehiclePedIsIn(ped, false), -1)==ped do
            Citizen.Wait(100)
        end
        toggleMenu(false,true)
    else
        return
    end
end)

RegisterNetEvent("tuning:closeMenu")
AddEventHandler("tuning:closeMenu",function()
    toggleMenu(false,true)
end)

local lastVeh = false
local lastData = false
local gotOut = false
Citizen.CreateThread(function(...)
    while not ESX do Citizen.Wait(0); end
    while not ESX.IsPlayerLoaded() do Citizen.Wait(0); end
    while true do
        Citizen.Wait(30)
        if IsPedInAnyVehicle(GetPlayerPed(-1)) then
            local veh = GetVehiclePedIsIn(GetPlayerPed(-1),false)
            if veh ~= lastVeh or gotOut then
                if gotOut then gotOut = false; end
                local responded = false
                ESX.TriggerServerCallback('tuning:CheckStats', function(doTune,stats)
                    if doTune then
                        setVehData(veh,stats)
                        lastStats = stats
                    else
                        if lastVeh and veh and lastVeh == veh and lastData then
                            setVehData(veh,lastData)
                        end
                    end
                    lastVeh = veh
                    responded = true
                end, ESX.Game.GetVehicleProperties(veh))
                while not responded do Citizen.Wait(0); end
            end
        else
            if not gotOut then
                gotOut = true
            end
        end
    end
end)