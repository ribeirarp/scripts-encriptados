$(document).ready(function(){
  // Mouse Controls
  var documentWidth = document.documentElement.clientWidth;
  var documentHeight = document.documentElement.clientHeight;
  var cursor = $('#cursor');
  var cursorX = documentWidth / 2;
  var cursorY = documentHeight / 2;

  function UpdateCursorPos() {
      $('#cursor').css('left', cursorX+2);
      $('#cursor').css('top', cursorY+2);
  }

  function triggerClick(x, y) {
      var element = $(document.elementFromPoint(x, y));
      element.focus().click();
      return true;
  }

  // Partial Functions
  function closeMain() {
    $(".body").fadeOut(100); 
    $(".meter-container").fadeOut(100); 
  }

  function closeAll() {
    $(".body").fadeOut(100); 
    $(".meter-container").fadeOut(100); 
  }
  

  // Listen for NUI Events
  window.addEventListener('message', function(event){
    var item = event.data;

    if(item.openSection == "avisar") {
      $(".meter-container").fadeIn(100); 
      $(".meter-container").animate({left: '21.30%'}, 1000, 'swing');
      var messagem = item.message
      $(".msg").empty();
      $(".msg").prepend(messagem);
    }

    if(item.openSection == "closeMessage") {
      //$(".meter-container").css("display", "none");
      $(".meter-container").animate({left: '-25%'});
    }

  });

  $(document).mousemove(function(event) {
    cursorX = event.pageX;
    cursorY = event.pageY;
    UpdateCursorPos();
  });

});
