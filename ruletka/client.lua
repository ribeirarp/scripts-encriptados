ESX = nil
Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj)
			ESX = obj 
		end)
		Citizen.Wait(0)
	end
end)
local blipX = -1400.94
local blipY = -605.00
local blipZ = 29.50
local pic = 'CHAR_SOCIAL_CLUB'
local game_during = false
local elements = {}

function DisplayHelpText(str)
    SetTextComponentFormat("STRING")
    AddTextComponentString(str)
    DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

RegisterNetEvent('r68:start')
AddEventHandler('r68:start', function()
	ESX.TriggerServerCallback('esx_r:check_money', function(quantity)
		if quantity >= 10 then
			SendNUIMessage({
				type = "show_table",
				zetony = quantity
			})
			SetNuiFocus(true, true)
		elseif quantity > 10000 then
			exports['mythic_notify']:SendAlert('error', 'Não podes jogar com tantos chips!!')
			SendNUIMessage({
				type = "reset_bet"
			})
		else
			--ESX.ShowNotification('Tens de ter no minimo 10 chips para jogar!!')
			exports['mythic_notify']:SendAlert('error', 'Tens de ter no minimo 10 chips para jogar!!')
			SendNUIMessage({
				type = "reset_bet"
			})
		end
	end, '')
end)

RegisterNUICallback('exit', function(data, cb)
	cb('ok')
	SetNuiFocus(false, false)
end)

RegisterNUICallback('betup', function(data, cb)
	cb('ok')
	TriggerServerEvent('InteractSound_SV:PlayOnSource', 'betup', 1.0)
end)

RegisterNUICallback('roll', function(data, cb)
	cb('ok')
	TriggerEvent('esx_roulette:start_game', data.kolor, data.kwota)
end)

RegisterNetEvent('esx_roulette:start_game')
AddEventHandler('esx_roulette:start_game', function(action, amount)
	local amount = amount
	
	if game_during == false then
		if amount > 50000 then
			exports['mythic_notify']:SendAlert('inform', 'Não podes apostar mais que 50000')

		else
			TriggerServerEvent('esx_r:removemoney', amount)
			local kolorBetu = action
			--TriggerEvent('pNotify:SendNotification', {text = "Apostaste "..amount.." fichas no "..kolorBetu..". A roda está girando ..."})
			exports['mythic_notify']:SendAlert('inform', 'Apostaste '..amount..' fichas no '..kolorBetu..'. A roda está girando ...')
			game_during = true
			local red = {32,19,21,25,34,27,36,30,23,5,16,1,14,9,18,7,12,3};
			local black = {15,4,2,17,6,13,11,8,10,24,33,20,31,22,29,28,35,26};
			local randomNumber = 0
			if math.random(0,36) == 0 and kolorBetu == "green" then
				randomNumber = 0
			else
				if kolorBetu == "red" then
					local randN = math.random(0,10)
					if randN >= 5 then
						randomNumber = black[math.random(1,#black)]
					else
						randomNumber = red[math.random(1,#black)]
					end
				else
					local randN = math.random(0,10)
					if randN >= 5 then
						randomNumber = red[math.random(1,#black)]
					else
						randomNumber = black[math.random(1,#black)]
					end
				end
			end
			
			--local randomNumber = 0
			SendNUIMessage({
				type = "show_roulette",
				hwButton = randomNumber
			})
			TriggerServerEvent('InteractSound_SV:PlayOnSource', 'ruletka', 1.0)
			Citizen.Wait(10000)
			
			print()	
			local function has_value (tab, val)
				for index, value in ipairs(tab) do
					if value == val then
						return true
					end
				end
				return false
			end
			if action == 'black' then
				if has_value(black, randomNumber) then
					local win = amount * 2
					--ESX.ShowNotification('Você ganhou'..win..' Fichas!')
					exports['mythic_notify']:SendAlert('success', 'Você ganhou '..win..' Fichas!')
					TriggerServerEvent('esx_r:givemoney', action, amount)
				else
					--ESX.ShowNotification('Não dessa vez. Tente novamente! Boa sorte!')
					exports['mythic_notify']:SendAlert('error', 'Não foi desta. Tente novamente! Boa sorte!')
				end
			elseif action == 'red' then
				local win = amount * 2
				if has_value(red, randomNumber) then
					--ESX.ShowNotification('Você ganhou '..win..' Fichas!')
					exports['mythic_notify']:SendAlert('success', 'Você ganhou '..win..' Fichas!')
					TriggerServerEvent('esx_r:givemoney', action, amount)
				else
					--ESX.ShowNotification('Não dessa vez. Tente novamente! Boa sorte!')
					exports['mythic_notify']:SendAlert('error', 'Não foi desta. Tente novamente! Boa sorte!')
				end
			elseif action == 'green' then
				local win = amount * 14
				if randomNumber == 0 then
					--ESX.ShowNotification('Você ganhou '..win..' Fichas!')
					exports['mythic_notify']:SendAlert('success', 'Você ganhou '..win..' Fichas!')
					TriggerServerEvent('esx_r:givemoney', action, amount)
				else
					--ESX.ShowNotification('Não dessa vez. Tente novamente! Boa sorte!')
					exports['mythic_notify']:SendAlert('error', 'Não foi desta. Tente novamente! Boa sorte!')
				end
			end
			--TriggerServerEvent('roulette:givemoney', randomNumber)
			SendNUIMessage({type = 'hide_roulette'})
			SetNuiFocus(false, false)
			--ESX.ShowNotification('Gra end!')
			game_during = false
			TriggerEvent('r68:start')

		end
		
	else
		--ESX.ShowNotification('A roda está girando ...')
		exports['mythic_notify']:SendAlert('inform', 'A roda está girando ...')
	end
end)